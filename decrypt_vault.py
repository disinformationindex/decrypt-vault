from binascii import unhexlify
import os
import sys

from cryptography.hazmat.primitives.hashes import SHA256
from cryptography.hazmat.primitives.hmac import HMAC
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives.padding import PKCS7
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers.algorithms import AES
from cryptography.hazmat.primitives.ciphers.modes import CTR
from cryptography.hazmat.primitives.ciphers import Cipher


def run(input_file, password):
	with open(input_file, "rb") as fh:
		b_header = fh.readline()
		b_cipher = b"".join(s.rstrip(b"\n") for s in fh.readlines())
	headers = [s.strip() for s in b_header.strip().split(b";")]
	prefix, version, cipher = headers
	assert headers[0] == b"$ANSIBLE_VAULT"
	assert headers[1] in (b"1.1", b"1.2")
	assert headers[2] == b"AES256"

	backend = default_backend()
	cipher = unhexlify(b_cipher)
	b_salt, b_hmac, b_text = cipher.split(b"\n", 2)
	b_salt = unhexlify(b_salt)
	b_hmac = unhexlify(b_hmac)
	b_text = unhexlify(b_text)

	kdf = PBKDF2HMAC(
		algorithm = SHA256(),
		length = 80,
		salt = b_salt,
		iterations = 10000,
		backend = backend,
	)
	b_derivedkey = kdf.derive(password.encode())
	b_key1 = b_derivedkey[:32]
	b_key2 = b_derivedkey[32:64]
	b_iv = b_derivedkey[64:80]

	hmac = HMAC(b_key2, SHA256(), backend)
	hmac.update(b_text)
	hmac.verify(b_hmac)

	cipher = Cipher(AES(b_key1), CTR(b_iv), backend)
	decryptor = cipher.decryptor()
	b_decrypted = decryptor.update(b_text) + decryptor.finalize()
	unpadder = PKCS7(128).unpadder()
	b_plaintext = unpadder.update(b_decrypted) + unpadder.finalize()
	return b_plaintext.decode("utf-8")


if __name__ == "__main__":
	password = os.environ.get("VAULT_PASSWORD")
	assert password, "No VAULT_PASSWORD ENV variable set"
	result = run(sys.argv[1], password)
	print(result)
