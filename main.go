package main

import (
	"bufio"
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"os"
	"strings"
	"golang.org/x/crypto/pbkdf2"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

type VaultEnvelope struct {
	Scanner *bufio.Scanner
	HeaderChecked bool
}

func (envelope *VaultEnvelope) CheckHeader() {
	scanner := envelope.Scanner
	scanner.Scan()
	header := scanner.Text()
	pieces := strings.SplitN(header, ";", 3)
	if pieces[0] != "$ANSIBLE_VAULT" {
		fmt.Println(pieces[0])
		panic("Invalid header")
	}
	if pieces[1] != "1.1" && pieces[1] != "1.2" {
		fmt.Println(pieces[1])
		panic("Invalid header")
	}
	if pieces[2] != "AES256" {
		fmt.Println(pieces[2])
		panic("Invalid header")
	}
	envelope.HeaderChecked = true
}

func (envelope *VaultEnvelope) ReadBody() *bytes.Buffer {
	buf := bytes.Buffer{}
	scanner := envelope.Scanner
	for scanner.Scan() {
		line := scanner.Text()
		decoded, err := hex.DecodeString(line)
		check(err)
		_, err = buf.Write(decoded)
		check(err)
	}
	return &buf
}

func newEnvelope(fh *os.File) *VaultEnvelope {
	scanner := bufio.NewScanner(fh)
	scanner.Split(bufio.ScanLines)
	return &VaultEnvelope{Scanner: scanner}
}

type VaultKDF struct {
	Key1 []byte
	Key2 []byte
	Iv []byte
}

func (kdf *VaultKDF) Decrypt(cipherText []byte) []byte {
	l := len(cipherText)
	buf := make([]byte, l)
	block, err := aes.NewCipher(kdf.Key1)
	check(err)
	stream := cipher.NewCTR(block, kdf.Iv)
	stream.XORKeyStream(buf, cipherText)
	return buf
}

func readKDF(password string, salt []byte, hmac []byte) *VaultKDF {
	kdf := pbkdf2.Key([]byte(password), salt, 10000, 80, sha256.New)
	return &VaultKDF{
		Key1: kdf[:32],
		Key2: kdf[32:64],
		Iv: kdf[64:80],
	}
}

type VaultDocument struct {
	Scanner *bufio.Scanner
	Salt []byte
	HMAC []byte
	Cipher []byte
}

func (document *VaultDocument) Parse() {
	var err error
	scanner := document.Scanner
	scanner.Scan()
	document.Salt, err = hex.DecodeString(scanner.Text())
	check(err)
	scanner.Scan()
	document.HMAC, err = hex.DecodeString(scanner.Text())
	check(err)
	scanner.Scan()
	document.Cipher, err = hex.DecodeString(scanner.Text())
	check(err)
}

func newVault(buf *bytes.Buffer) *VaultDocument {
	scanner := bufio.NewScanner(buf)
	scanner.Split(bufio.ScanLines)
	return &VaultDocument{Scanner: scanner}
}

func UnPKCS7Padding(b []byte) []byte {
	l := len(b)
	if b == nil || l == 0 {
		panic("Invalid PKCS7")
	}
	c := b[l - 1]
	n := int(c)
	if n == 0 {
		panic("Padding invalid")
	}
	for i := 0; i < n; i++ {
		if b[l - n + i] != c {
			panic("Issue unpadding")
		}
	}
	return b[:l - n]
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("No file argument")
		return
	}
	password := os.Getenv("VAULT_PASSWORD")

	fh, err := os.Open(os.Args[1])
	check(err)

	envelope := newEnvelope(fh)
	envelope.CheckHeader()
	body := envelope.ReadBody()

	vault := newVault(body)
	vault.Parse()

	kdf := readKDF(password, vault.Salt, vault.HMAC)
	plaintext := kdf.Decrypt(vault.Cipher)
	unpadded := UnPKCS7Padding(plaintext)
	fmt.Println(string(unpadded))
}
